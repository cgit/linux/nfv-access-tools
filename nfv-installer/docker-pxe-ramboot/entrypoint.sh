#!/usr/bin/env bash
echo "Starting DHCP on interface: $1"

/usr/sbin/dhcpd --no-pid $1
nohup in.tftpd -L&
/bin/bash
